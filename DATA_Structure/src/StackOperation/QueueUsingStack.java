/**
 * write a program to perform queue operation using stack
 * take five in puts from user and if extra valu insert itremove
 * and extra value add at last index
 * *@author  Krisha
 *@version 	 1.0
 *since       7-jully-2017
 */
package StackOperation;
import java.util.Scanner;
import java.util.Stack;

//this class will implement queue using stackpublic class QueueUsingStack
public class QueueUsingStack {
	public static void main(String[] args) {
		Stack inputstack = new Stack();

		Scanner scanner = new Scanner(System.in);

		int choice = 0;
		do {

			System.out.println("1. Enqueue \n 2. Quit");
			System.out.print("Select from the options: ");
			choice = scanner.nextInt();
			switch (choice) {
			case 1:
				if (inputstack.size() < 5)// check if stack size is less than
											// or equal to 5
				{
					System.out.print("Enter Value to Enqueue: ");
					int addvalue = scanner.nextInt();
					inputstack.push(addvalue);// pushing value in stack
					System.out.println("Queue: " + inputstack);
				} 
				else// when stack size is greater than 5
				{
					inputstack.remove(0);
					System.out.print("Enter Value to Enqueue: ");
					int addvalue = scanner.nextInt();
					inputstack.push(addvalue);// pushing value in stack
					System.out.println("Queue: " + inputstack);
				}
				break;

			case 2:
				System.out.println("You have choose to quit");
				break;
			}
		} 
		while (choice != 2); // break when choice is equal to 2
		System.out.println("logs: do-while loops break");
		System.out.println("logs: exiting main");
	}
}
