//  Program

//	Write code to insert BST(binary Search tree)
//	class to to create Node
//  Constructor for initialize node to null
//	function to insert key in BST
//	Display to print  BST


package BST;
import java.util.Scanner;
public class bst 
{
	
	// class to create a node to right ,left and key values

	class node     
	{
		int key;
		node left , right ;
	  
	  public node(int item) 
	  {
          key = item;
          left = right = null;
      }
	  
	}
		//root of BST
    node root;
//constructor for initialize node to null
	bst() 
	{
	root=null;	
	}
	
	
//function to insert key in BST	
void add(int key)
	{
		root=insert(root,key);
		
	}
	
	 node insert(node root, int key) {
	        if (root == null) {
	            root = new node(key);
	            return root;
	        }
	 
	       
	        if (key < root.key)
	            root.left = insert(root.left, key);
	        else if (key > root.key)
	            root.right = insert(root.right, key);
	 
	       
	        return root;
	    }
	 
	 
	 void display()  
	 {
	       insequence(root);
	 }
	 
	    void insequence(node root) {
	        if (root != null) {
	            insequence(root.left);
	            System.out.println(root.key);
	            insequence(root.right);
	        }
	    }
	

	public static void main(String[] args) 
	{
		
		bst BinarySsearchTree =new bst();
	
		Scanner scanobj= new Scanner(System.in);
		System.out.println("enter the how many no you want to write");
		int number=scanobj.nextInt();
		for(int i=1;i<=number;i++)
		{
			System.out.println("enter value "+i+"= ");
		int data =scanobj.nextInt();
		BinarySsearchTree.add(data);

		}		
		BinarySsearchTree.display();
	}

}

